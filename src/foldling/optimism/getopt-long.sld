;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Command line argument handling for R7RS Scheme.
;;;
;;; See README and optimism.sld for more information.
;;;
;;; This software is written by Evan Hanson <evhan@foldling.org> and
;;; placed in the Public Domain. All warranties are disclaimed.
;;;

;;
;; This Scheme library provides a `getopt_long(3)`-style option handler
;; for the `(foldling optimism)` library.
;;
;; It provides a function, `getopt-long`, that can be used as a matcher
;; for that library's `parse-command-line` procedure, as well as a
;; specialized version of `parse-command-line`.
;;
;; Refer to the README for more details.
;;
(define-library (foldling optimism getopt-long)
  (import (scheme base)
          (scheme case-lambda)
          (scheme process-context)
          (rename (foldling optimism) (parse-command-line parse))
          (except (foldling optimism getopt) parse-command-line))
  (export getopt-long parse-command-line)
  (include "getopt-long.scm")
  (begin

    ;; While getopt-long.scm defines a matcher for double-dashed options
    ;; only, the `getopt-long` procedure exported by this library
    ;; implements the combined behavior of that and the standard
    ;; (single-dashed) `getopt` matcher.
    (define getopt-long
      (let ((getopt-long getopt-long))
        (lambda (arg grammar)
          (or (getopt-long arg grammar)
              (getopt arg grammar)))))

    ;; A staged parser that implicitly uses the `getopt-long` matcher.
    (define parse-command-line
      (case-lambda
        ((grammar)
         (parse getopt-long (cdr (command-line)) grammar))
        ((arguments grammar)
         (parse getopt-long arguments grammar))))))
