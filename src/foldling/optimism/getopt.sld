;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Command line argument handling for R7RS Scheme.
;;;
;;; See README and optimism.sld for more information.
;;;
;;; This software is written by Evan Hanson <evhan@foldling.org> and
;;; placed in the Public Domain. All warranties are disclaimed.
;;;

;;
;; This Scheme library provides a `getopt(3)`-style option handler for
;; the `(foldling optimism)` library.
;;
;; It provides a function, `getopt`, that can be used as a matcher for
;; that library's `parse-command-line` procedure, as well as a
;; specialized version of `parse-command-line`.
;;
;; Refer to the README for more details.
;;
(define-library (foldling optimism getopt)
  (import (scheme base)
          (scheme case-lambda)
          (scheme process-context)
          (rename (foldling optimism)
                  (parse-command-line parse)))
  (export getopt parse-command-line)
  (include "getopt.scm")
  (begin

    ;; A staged parser that implicitly uses the `getopt` matcher.
    (define parse-command-line
      (case-lambda
        ((grammar)
         (parse getopt (cdr (command-line)) grammar))
        ((arguments grammar)
         (parse getopt arguments grammar))))))
