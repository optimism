;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Command line argument handling for CHICKEN Scheme.
;;;
;;; See README and optimism.sld for more information.
;;;
;;; This software is written by Evan Hanson <evhan@foldling.org> and
;;; placed in the Public Domain. All warranties are disclaimed.
;;;

(module (optimism getopt-long) (getopt-long parse-command-line)
  (import (except (scheme) string-copy)
          (chicken base)
          (chicken process-context)
          (rename (optimism) (parse-command-line parse))
          (except (optimism getopt) parse-command-line))
  (include "src/chicken/r7rs.scm")
  (include "src/foldling/optimism/getopt-long.scm")
  (include "src/foldling/optimism/getopt-long.sld"))
