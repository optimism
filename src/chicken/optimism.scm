;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Command line argument handling for CHICKEN Scheme.
;;;
;;; See README and optimism.sld for more information.
;;;
;;; This software is written by Evan Hanson <evhan@foldling.org> and
;;; placed in the Public Domain. All warranties are disclaimed.
;;;

(module (optimism) (parse-command-line)
  (import (except (scheme) string-copy)
          (chicken base)
          (chicken process-context))
  (include "src/chicken/r7rs.scm")
  (include "src/foldling/optimism.scm"))
