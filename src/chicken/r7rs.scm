;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; R7RS shims for optimism.scm.
;;;
;;; See README and optimism.sld for more information.
;;;
;;; This software is written by Evan Hanson <evhan@foldling.org> and
;;; placed in the Public Domain. All warranties are disclaimed.
;;;

(define (command-line)
  (cons (program-name)
        (command-line-arguments)))

(define (string-copy s i j)
  (substring s i j))

(define-syntax define-library
  (syntax-rules (begin)
    ((_ spec ... (begin . body))
     (begin . body))))
