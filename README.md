optimism
========
Command line option handling for R7RS Scheme.

Description
-----------
This Scheme library provides a way to collect command line options into
an association list according to a simple S-expressive grammar. It's
meant to be easy to use and pure-R7RS.

Usage
-----
One procedure is provided, `parse-command-line`, which takes as
arguments an options grammar and an optional list of command line option
strings and returns an association list. If no list of options is given,
the program's command line arguments are used.

    (import (foldling optimism))
    
    (parse-command-line
     '("-a" "-b" "one" "-c" "two" "three" "-d" "four" "5" "six")
     `((-a)
       (-b . foo)
       (-c bar baz)
       (-d ,string->symbol ,string->number)))
    
    ; => ((-a)
    ;     (-b . "one")
    ;     (-c "two" "three")
    ;     (-d four 5)
    ;     (-- "six"))

As a special case, an item in the grammar whose first element is a list
will be split into separate entries, allowing an abbreviated syntax for
multiple option specifications of the same form:

    (parse-command-line
     '(((--foo --bar --baz) . qux)))
    
    ; => (parse-command-line
    ;     '((--foo . qux)
    ;       (--bar . qux)
    ;       (--baz . qux)))

Parsers for `getopt(3)` and `getopt_long(3)`-style command lines are
provided by the `getopt` and `getopt-long` sublibraries, respectively.

    (import (foldling optimism getopt))
    
    (parse-command-line               ; => ((-a)
     '("-abcone" "-d" "two")          ;     (-b)
     '((-a)                           ;     (-c . "one")
       (-b)                           ;     (-d . "two")
       (-c . one)                     ;     (--))
       (-d . two)))

    (import (foldling optimism getopt-long))
    
    (parse-command-line               ; => ((-a . "one")
     '("-aone" "--foo" "--bar=two")   ;     (--foo)
     '((-a . one)                     ;     (--bar . "two")
       (--foo)                        ;     (--))
       (--bar . two)))

Refer to `optimism.scm` for more API details.

Installation
------------
To install for CHICKEN Scheme, run `chicken-install` from the project's
root directory.

    $ git clone http://git.foldling.org/optimism.git
    $ cd optimism
    $ chicken-install -test

Other Schemes should install the following files:

    src/foldling/optimism.scm
    src/foldling/optimism.sld
    src/foldling/optimism/getopt.scm
    src/foldling/optimism/getopt.sld
    src/foldling/optimism/getopt-long.scm
    src/foldling/optimism/getopt-long.sld

Author
------
Evan Hanson <evhan@foldling.org>

License
-------
This software is written by Evan Hanson and placed in the Public
Domain. All warranties are disclaimed.
